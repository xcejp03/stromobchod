package cz.inventi.stromObchod.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "/shop")
public class ShopController {

    @RequestMapping(value = "/menu", method = RequestMethod.GET)
    public String categories() {

        return "shop";
    }
}
