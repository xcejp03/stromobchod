package cz.inventi.stromObchod.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kbc.nase.mtb.ra.oh.model.persist.Book;
import cz.inventi.stromObchod.util.RestApiSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/rest")
public class RestController {

    @Autowired
    private RestApiSupport restApiSupport;

    @Autowired
    private ObjectMapper mapper;

    @RequestMapping(method = RequestMethod.GET)
    public String jackson(Model model) {
        String prettyJson = null;
        ResponseEntity responseEntity = restApiSupport.callRestGet(Book.class);
        try {
            prettyJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(responseEntity.getBody());
            model.addAttribute("book", prettyJson);
        } catch (JsonProcessingException e) {
            model.addAttribute("book", responseEntity.getBody());
            e.printStackTrace();
        }

        return "rest";

    }

}
