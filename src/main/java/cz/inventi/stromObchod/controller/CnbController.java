package cz.inventi.stromObchod.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import cz.inventi.stromObchod.util.RestApiSupport;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.*;
import java.net.URL;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/cnb")
public class CnbController {

    private final Logger l = Logger.getLogger(this.getClass());

    @Autowired
    private RestApiSupport restApiSupport;

    @Autowired
    private ObjectMapper mapper;

    @RequestMapping(method = RequestMethod.GET)
    public String cnb(Model model) throws IOException {
        URL website = new URL("http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt");
        String target = "C:/workspace/xxx/kurzy.txt";
        InputStream in = website.openStream();

        String result = "";
        try {
            result = new BufferedReader(new InputStreamReader(in))
                    .lines().collect(Collectors.joining("\r\n"));
            l.info("result: " + result);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try (PrintWriter out = new PrintWriter(target)) {
            out.print(result);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "cnb";
    }

}
