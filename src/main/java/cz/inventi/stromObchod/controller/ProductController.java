package cz.inventi.stromObchod.controller;

import cz.inventi.stromObchod.DTO.CreateProductForm;
import cz.inventi.stromObchod.entity.Product;
import cz.inventi.stromObchod.entity.ProductCategoryEnum;
import cz.inventi.stromObchod.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping(value = "/product")
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String createProduct(Model model, CreateProductForm createProductForm) {
        return "productCreate";
    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createProduct(Model model, @Valid CreateProductForm createProductForm, BindingResult bindingResult, HttpServletRequest request) {
        System.out.println("createProductForm: " + createProductForm);

        if (createProductForm != null) {
            System.out.println("form is not null - will save");
            productRepository.save(new Product()
                    .setName(createProductForm.getName())
                    .setDescription(createProductForm.getDescription())
                    .setProductCategory(createProductForm.getProductCategory())
                    .setPrice(createProductForm.getPrice()));
            System.out.println("saved");
        }
        return "success";
    }

    @RequestMapping(value = "/category/{id}", method = RequestMethod.GET)
    public String productsByCategory(Model model, @PathVariable("id") String id) {
        List<Product> products = productRepository.findByProductCategory(ProductCategoryEnum.valueOf(id.toUpperCase()));
        System.out.println(products);
        model.addAttribute("products", products);
        return "products";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String productById(Model model, @PathVariable("id") Long id) {
        Product product = productRepository.findById(id).get();
        System.out.println(product);
        model.addAttribute("product", product);
        return "productDetail";
    }




}
