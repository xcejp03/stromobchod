package cz.inventi.stromObchod.controller;

import cz.inventi.stromObchod.entity.Basket;
import cz.inventi.stromObchod.entity.Person;
import cz.inventi.stromObchod.entity.PersonRoleEnum;
import cz.inventi.stromObchod.entity.Product;
import cz.inventi.stromObchod.repository.BasketRepository;
import cz.inventi.stromObchod.repository.PersonRepository;
import cz.inventi.stromObchod.repository.ProductRepository;
import cz.inventi.stromObchod.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/basket")
public class BasketController {
    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private BasketRepository basketRepository;


    @Autowired
    private ProductRepository productRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String showBasket(Model model) {
        System.out.println("show basket");
        Person person = personRepository.findById(Constants.PERSON_ID).get();
        Basket basket = basketRepository.findById(person.getBasket().id).get();
        model.addAttribute("basket", basket);
        return "basket";
    }

    @RequestMapping(value = "/add/{id}", method = RequestMethod.GET)
    public String addProduct(@PathVariable("id") Long id, HttpServletRequest request) {
        System.out.println("add product");
        Person person = personRepository.findById(Constants.PERSON_ID).get();
        Basket basket = basketRepository.findById(person.getBasket().id).get();
        Product product = productRepository.findById(id).get();

        basket.getProducts().add(product);

        basketRepository.save(basket);

        String referer = request.getHeader("Referer");
        System.out.println("referer: " + referer);

        if (referer != null) {
            return "redirect:" + referer;
        } else {
            return "success";
        }
    }

    @RequestMapping(value = "/remove/{id}", method = RequestMethod.GET)
    public String removeProduct(@PathVariable("id") Long id, HttpServletRequest request) {
        System.out.println("remove product");
        Person person = personRepository.findById(Constants.PERSON_ID).get();
        Basket basket = basketRepository.findById(person.getBasket().id).get();
        Product product = productRepository.findById(id).get();

        basket.getProducts().remove(product);

        basketRepository.save(basket);

        String referer = request.getHeader("Referer");
        System.out.println("referer: " + referer);

        if (referer != null) {
            return "redirect:" + referer;
        } else {
            return "success";
        }
    }

}
