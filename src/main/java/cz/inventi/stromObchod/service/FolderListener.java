package cz.inventi.stromObchod.service;

import org.apache.log4j.Logger;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.*;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;

@Component
public class FolderListener {

    private final Logger l = Logger.getLogger(this.getClass());

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    Job job;

    @Bean
    @Async
    public String listen() throws IOException, InterruptedException {
        l.info("Folder listener started");
        WatchService watcher = FileSystems.getDefault().newWatchService();
        Path dir = Paths.get("C:/workspace/xxx/");
        try {
            WatchKey key = dir.register(watcher, ENTRY_CREATE);
        } catch (IOException x) {
            System.err.println(x);
        }

        WatchKey key;
        while ((key = watcher.take()) != null) {
            for (WatchEvent<?> event : key.pollEvents()) {
                l.info("Event kind:" + event.kind() + ". File affected: " + event.context() + ".");
                try {
                    JobParameters jobParameters = new JobParametersBuilder().addLong("time", System.currentTimeMillis())
                            .toJobParameters();
                    l.info("Will start job");
                    jobLauncher.run(job, jobParameters);
                } catch (Exception e) {
                    l.info(e.getMessage());
                }
            }
            key.reset();
        }

        return "done";
    }

}
