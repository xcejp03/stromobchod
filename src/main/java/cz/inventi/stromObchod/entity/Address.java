package cz.inventi.stromObchod.entity;

import javax.persistence.Entity;

@Entity
public class Address extends BaseEntity {

    private String city;

    private String street;

    public String getCity() {
        return city;
    }

    public Address setCity(String city) {
        this.city = city;
        return this;
    }

    public String getStreet() {
        return street;
    }

    public Address setStreet(String street) {
        this.street = street;
        return this;
    }
}
