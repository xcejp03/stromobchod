package cz.inventi.stromObchod.entity;

public enum Shipping {
    CP, PPL, DPD
}
