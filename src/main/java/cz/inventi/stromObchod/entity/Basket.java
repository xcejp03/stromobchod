package cz.inventi.stromObchod.entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class Basket extends BaseEntity {

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinTable(name = "BASKET_PRODUCT",
            joinColumns = @JoinColumn(name = "PRODUCT_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "basket_ID", referencedColumnName = "ID"))
    List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public Basket setProducts(List<Product> products) {
        this.products = products;
        return this;
    }
}
