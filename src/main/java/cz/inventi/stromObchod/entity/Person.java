package cz.inventi.stromObchod.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.List;

@Entity
public class Person extends BaseEntity {

    private String email;

    private String surname;

    private String lastname;

    private PersonRoleEnum personRoleEnum;

    @OneToOne
    private Basket basket;

    @OneToMany
    @JoinColumn(name = "order_id")
    private List<Ord> orders;

    @OneToOne
    private Address invoiceAddress;

    @OneToOne
    private Address shippingAddress;

    public String getEmail() {
        return email;
    }

    public Person setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public Person setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getLastname() {
        return lastname;
    }

    public Person setLastname(String lastname) {
        this.lastname = lastname;
        return this;
    }

    public PersonRoleEnum getPersonRoleEnum() {
        return personRoleEnum;
    }

    public Person setPersonRoleEnum(PersonRoleEnum personRoleEnum) {
        this.personRoleEnum = personRoleEnum;
        return this;
    }

    public Basket getBasket() {
        return basket;
    }

    public Person setBasket(Basket basket) {
        this.basket = basket;
        return this;
    }

    public List<Ord> getOrders() {
        return orders;
    }

    public Person setOrders(List<Ord> orders) {
        this.orders = orders;
        return this;
    }

    public Address getInvoiceAddress() {
        return invoiceAddress;
    }

    public Person setInvoiceAddress(Address invoiceAddress) {
        this.invoiceAddress = invoiceAddress;
        return this;
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public Person setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
        return this;
    }
}
