package cz.inventi.stromObchod.entity;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
public class Product extends BaseEntity {

    private String name;

    private String description;

    private ProductCategoryEnum productCategory;

    private BigDecimal price;

    private String img;

    public String getName() {
        return name;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public Product setDescription(String description) {
        this.description = description;
        return this;
    }

    public ProductCategoryEnum getProductCategory() {
        return productCategory;
    }

    public Product setProductCategory(ProductCategoryEnum productCategory) {
        this.productCategory = productCategory;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Product setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    public String getImg() {
        return img;
    }

    public Product setImg(String img) {
        this.img = img;
        return this;
    }
}
