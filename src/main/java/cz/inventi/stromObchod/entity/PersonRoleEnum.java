package cz.inventi.stromObchod.entity;

public enum PersonRoleEnum {
    ADMIN, CUSTOMER
}
