package cz.inventi.stromObchod.entity;

import javax.persistence.Entity;

@Entity
public class People extends BaseEntity {


    private String lastName;
    private String firstName;

    public People() {
    }


    public People(String lastName, String firstName) {
        this.lastName = lastName;
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public People setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public People setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }


    @Override
    public String toString() {
        return "People{" +
                "lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                '}';
    }
}
