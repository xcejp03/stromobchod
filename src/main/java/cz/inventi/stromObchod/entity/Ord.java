package cz.inventi.stromObchod.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.math.BigDecimal;
import java.util.List;
// předělat na order a tabulku název ord
@Entity
public class Ord extends BaseEntity {

    @OneToMany
    @JoinColumn(name = "product_id")
    private List<Product> products;

    @OneToOne
    private Address shippingAddress;

    private Shipping shipping;

    private OrderStateEnum orderStateEnum;

    private BigDecimal price;

    public List<Product> getProducts() {
        return products;
    }

    public Ord setProducts(List<Product> products) {
        this.products = products;
        return this;
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public Ord setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
        return this;
    }

    public Shipping getShipping() {
        return shipping;
    }

    public Ord setShipping(Shipping shipping) {
        this.shipping = shipping;
        return this;
    }

    public OrderStateEnum getOrderStateEnum() {
        return orderStateEnum;
    }

    public Ord setOrderStateEnum(OrderStateEnum orderStateEnum) {
        this.orderStateEnum = orderStateEnum;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Ord setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }
}
