package cz.inventi.stromObchod.entity.json;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Results {

    private String listName;

    private String list_name;

    private String displayName;

    private int rank;

    public String getListName() {
        return listName;
    }

    public Results setListName(String listName) {
        this.listName = listName;
        return this;
    }

    public String getList_name() {
        return list_name;
    }

    public Results setList_name(String list_name) {
        this.list_name = list_name;
        return this;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Results setDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public int getRank() {
        return rank;
    }

    public Results setRank(int rank) {
        this.rank = rank;
        return this;
    }
}
