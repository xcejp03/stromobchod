package cz.inventi.stromObchod.entity.json;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Book {

    private String status;

    private String copyright;

    private int numResult;

    private String lastModified;

    private String last_modified;

    private List<Results> results;

    public String getStatus() {
        return status;
    }

    public Book setStatus(String status) {
        this.status = status;
        return this;
    }

    public String getCopyright() {
        return copyright;
    }

    public Book setCopyright(String copyright) {
        this.copyright = copyright;
        return this;
    }

    public String getLastModified() {
        return lastModified;
    }

    public Book setLastModified(String lastModified) {
        this.lastModified = lastModified;
        return this;
    }

    public int getNumResult() {
        return numResult;
    }

    public Book setNumResult(int numResult) {
        this.numResult = numResult;
        return this;
    }

    public String getLast_modified() {
        return last_modified;
    }

    public Book setLast_modified(String last_modified) {
        this.last_modified = last_modified;
        return this;
    }


    public List<Results> getResults() {
        return results;
    }

    public Book setResults(List<Results> results) {
        this.results = results;
        return this;
    }
}
