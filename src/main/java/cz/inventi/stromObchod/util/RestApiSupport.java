package cz.inventi.stromObchod.util;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;

@Component
public class RestApiSupport {
    //    private static String URL_NYTIMES = "https://openexchangerates.org/api/latest.json";
//    private static String API_KEY_PARAM = "app_id";
//    String API_KEY_VALUE = "028bb9ea1f954967b14170fdba5096b5";
    private static String URL_NYTIMES = "https://api.nytimes.com/svc/books/v3/lists/best-sellers/history.json";
    private static String API_KEY_PARAM = "api-key";
    String API_KEY_VALUE = "eb0d706dc0aa42e7bb6715993595d2eb";


    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    public <R> ResponseEntity<R> callRestGet(Class<R> returnType) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(URL_NYTIMES)
                .queryParam(API_KEY_PARAM, API_KEY_VALUE);

        ResponseEntity<String> responseString;

        responseString = restTemplate.exchange(
                builder.toUriString(),
                HttpMethod.GET,
                null,
                String.class);

        String stringBody = responseString.getBody();

        R response = null;

        try {

            response = objectMapper.readValue(stringBody, returnType);

        } catch (IOException e) {
            e.printStackTrace();
        }
        ResponseEntity<R> responseEntity = new ResponseEntity<>(response, responseString.getHeaders(), responseString.getStatusCode());

        return responseEntity;

    }


}
