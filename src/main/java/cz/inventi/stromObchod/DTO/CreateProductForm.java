package cz.inventi.stromObchod.DTO;

import cz.inventi.stromObchod.entity.ProductCategoryEnum;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class CreateProductForm extends BaseDTO {

    @Size(min = 1, max = 200)
    private String name;

    @Size(min = 0, max = 500)
    private String description;

    @Nullable
    private ProductCategoryEnum productCategory;

    private BigDecimal price;

    @Nullable
    private String img;

    public String getName() {
        return name;
    }

    public CreateProductForm setName(String name) {
        this.name = name;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public CreateProductForm setDescription(String description) {
        this.description = description;
        return this;
    }

    @Nullable
    public ProductCategoryEnum getProductCategory() {
        return productCategory;
    }

    public CreateProductForm setProductCategory(@Nullable ProductCategoryEnum productCategory) {
        this.productCategory = productCategory;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public CreateProductForm setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    @Nullable
    public String getImg() {
        return img;
    }

    public CreateProductForm setImg(@Nullable String img) {
        this.img = img;
        return this;
    }

    @Override
    public String toString() {
        return "CreateProductForm{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", productCategory=" + productCategory +
                ", price=" + price +
                ", img='" + img + '\'' +
                '}';
    }
}
