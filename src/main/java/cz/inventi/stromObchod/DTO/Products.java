package cz.inventi.stromObchod.DTO;

import cz.inventi.stromObchod.entity.ProductCategoryEnum;
import org.springframework.lang.Nullable;

import javax.validation.constraints.Size;
import java.math.BigDecimal;

public class Products extends BaseDTO {

    @Size(min = 1, max = 200)
    private String name;


    private BigDecimal price;

    @Nullable
    private String img;


    public String getName() {
        return name;
    }

    public Products setName(String name) {
        this.name = name;
        return this;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public Products setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    @Nullable
    public String getImg() {
        return img;
    }

    public Products setImg(@Nullable String img) {
        this.img = img;
        return this;
    }
}
