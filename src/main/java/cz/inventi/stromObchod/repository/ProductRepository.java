package cz.inventi.stromObchod.repository;

import cz.inventi.stromObchod.entity.Product;
import cz.inventi.stromObchod.entity.ProductCategoryEnum;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    List<Product> findByProductCategory(ProductCategoryEnum productCategory);
}
