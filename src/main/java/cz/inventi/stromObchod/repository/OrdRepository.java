package cz.inventi.stromObchod.repository;

import cz.inventi.stromObchod.entity.Ord;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrdRepository extends CrudRepository<Ord, Long> {
}
