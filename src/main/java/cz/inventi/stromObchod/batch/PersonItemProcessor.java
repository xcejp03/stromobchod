package cz.inventi.stromObchod.batch;

import cz.inventi.stromObchod.entity.People;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;


public class PersonItemProcessor implements ItemProcessor<People, People> {

    private static final Logger log = LoggerFactory.getLogger(JobCompletionNotificationListener.class);

    @Override
    public People process(People people) throws Exception {
        log.info("xxx 6 - process");
        final String firstName = people.getFirstName().toUpperCase();
        final String lastName = people.getLastName().toUpperCase();

        final People transformedPerson = new People(firstName, lastName);


        return transformedPerson;
    }

}
