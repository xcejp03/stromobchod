package cz.inventi.stromObchod.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = Tree.NAME)
public class Tree {
    public static final String NAME = "Tree";

    @XmlElement(name = "ssd")
    private String jmeno;

    public String getJmeno() {
        return jmeno;
    }

    public Tree setJmeno(String jmeno) {
        this.jmeno = jmeno;
        return this;
    }
}
