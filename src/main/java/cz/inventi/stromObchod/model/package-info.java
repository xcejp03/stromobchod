// Define namespace for all classes in this package
@javax.xml.bind.annotation.XmlSchema(namespace = Namespaces.TREE, elementFormDefault = javax.xml.bind.annotation.XmlNsForm.QUALIFIED)
// We have annotation on class fields
@XmlAccessorType(XmlAccessType.FIELD)
package cz.inventi.stromObchod.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;