package cz.inventi.stromObchod.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = Flower.NAME, namespace = "http://inventi.cz/stromObchod/flower")
public class Flower {
    public static final String NAME = "Flower";

    @XmlElement(name = "sjdj")
    private String jmeno;

    public String getJmeno() {
        return jmeno;
    }

    public Flower setJmeno(String jmeno) {
        this.jmeno = jmeno;
        return this;
    }
}
