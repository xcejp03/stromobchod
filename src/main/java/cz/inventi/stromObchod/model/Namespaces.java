package cz.inventi.stromObchod.model;

public class Namespaces {

    public static final String BASE_NS = "http://inventi.cz/stromObchod";

    public static final String TREE = BASE_NS + "/Tree_v1";

}
