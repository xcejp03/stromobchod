/*
 * Books API
 * The Books API provides information about book reviews and The New York Times bestsellers lists.
 *
 * OpenAPI spec version: 3.0.0
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */


package com.kbc.nase.mtb.ra.oh.model.persist;

import java.util.Objects;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * BookRanksHistory
 */

public class BookRanksHistory {
  @JsonProperty("primary_isbn10")
  private String primaryIsbn10 = null;

  @JsonProperty("primary_isbn13")
  private String primaryIsbn13 = null;

  @JsonProperty("rank")
  private Integer rank = null;

  @JsonProperty("list_name")
  private String listName = null;

  @JsonProperty("display_name")
  private String displayName = null;

  @JsonProperty("published_date")
  private String publishedDate = null;

  @JsonProperty("bestsellers_date")
  private String bestsellersDate = null;

  @JsonProperty("weeks_on_list")
  private Integer weeksOnList = null;

  @JsonProperty("asterisk")
  private Integer asterisk = null;

  @JsonProperty("dagger")
  private Integer dagger = null;

  public BookRanksHistory primaryIsbn10(String primaryIsbn10) {
    this.primaryIsbn10 = primaryIsbn10;
    return this;
  }

   /**
   * Get primaryIsbn10
   * @return primaryIsbn10
  **/
  @ApiModelProperty(value = "")
  public String getPrimaryIsbn10() {
    return primaryIsbn10;
  }

  public void setPrimaryIsbn10(String primaryIsbn10) {
    this.primaryIsbn10 = primaryIsbn10;
  }

  public BookRanksHistory primaryIsbn13(String primaryIsbn13) {
    this.primaryIsbn13 = primaryIsbn13;
    return this;
  }

   /**
   * Get primaryIsbn13
   * @return primaryIsbn13
  **/
  @ApiModelProperty(value = "")
  public String getPrimaryIsbn13() {
    return primaryIsbn13;
  }

  public void setPrimaryIsbn13(String primaryIsbn13) {
    this.primaryIsbn13 = primaryIsbn13;
  }

  public BookRanksHistory rank(Integer rank) {
    this.rank = rank;
    return this;
  }

   /**
   * Get rank
   * @return rank
  **/
  @ApiModelProperty(value = "")
  public Integer getRank() {
    return rank;
  }

  public void setRank(Integer rank) {
    this.rank = rank;
  }

  public BookRanksHistory listName(String listName) {
    this.listName = listName;
    return this;
  }

   /**
   * Get listName
   * @return listName
  **/
  @ApiModelProperty(value = "")
  public String getListName() {
    return listName;
  }

  public void setListName(String listName) {
    this.listName = listName;
  }

  public BookRanksHistory displayName(String displayName) {
    this.displayName = displayName;
    return this;
  }

   /**
   * Get displayName
   * @return displayName
  **/
  @ApiModelProperty(value = "")
  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public BookRanksHistory publishedDate(String publishedDate) {
    this.publishedDate = publishedDate;
    return this;
  }

   /**
   * Get publishedDate
   * @return publishedDate
  **/
  @ApiModelProperty(value = "")
  public String getPublishedDate() {
    return publishedDate;
  }

  public void setPublishedDate(String publishedDate) {
    this.publishedDate = publishedDate;
  }

  public BookRanksHistory bestsellersDate(String bestsellersDate) {
    this.bestsellersDate = bestsellersDate;
    return this;
  }

   /**
   * Get bestsellersDate
   * @return bestsellersDate
  **/
  @ApiModelProperty(value = "")
  public String getBestsellersDate() {
    return bestsellersDate;
  }

  public void setBestsellersDate(String bestsellersDate) {
    this.bestsellersDate = bestsellersDate;
  }

  public BookRanksHistory weeksOnList(Integer weeksOnList) {
    this.weeksOnList = weeksOnList;
    return this;
  }

   /**
   * Get weeksOnList
   * @return weeksOnList
  **/
  @ApiModelProperty(value = "")
  public Integer getWeeksOnList() {
    return weeksOnList;
  }

  public void setWeeksOnList(Integer weeksOnList) {
    this.weeksOnList = weeksOnList;
  }

  public BookRanksHistory asterisk(Integer asterisk) {
    this.asterisk = asterisk;
    return this;
  }

   /**
   * Get asterisk
   * @return asterisk
  **/
  @ApiModelProperty(value = "")
  public Integer getAsterisk() {
    return asterisk;
  }

  public void setAsterisk(Integer asterisk) {
    this.asterisk = asterisk;
  }

  public BookRanksHistory dagger(Integer dagger) {
    this.dagger = dagger;
    return this;
  }

   /**
   * Get dagger
   * @return dagger
  **/
  @ApiModelProperty(value = "")
  public Integer getDagger() {
    return dagger;
  }

  public void setDagger(Integer dagger) {
    this.dagger = dagger;
  }


  @Override
  public boolean equals(java.lang.Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    BookRanksHistory bookRanksHistory = (BookRanksHistory) o;
    return Objects.equals(this.primaryIsbn10, bookRanksHistory.primaryIsbn10) &&
        Objects.equals(this.primaryIsbn13, bookRanksHistory.primaryIsbn13) &&
        Objects.equals(this.rank, bookRanksHistory.rank) &&
        Objects.equals(this.listName, bookRanksHistory.listName) &&
        Objects.equals(this.displayName, bookRanksHistory.displayName) &&
        Objects.equals(this.publishedDate, bookRanksHistory.publishedDate) &&
        Objects.equals(this.bestsellersDate, bookRanksHistory.bestsellersDate) &&
        Objects.equals(this.weeksOnList, bookRanksHistory.weeksOnList) &&
        Objects.equals(this.asterisk, bookRanksHistory.asterisk) &&
        Objects.equals(this.dagger, bookRanksHistory.dagger);
  }

  @Override
  public int hashCode() {
    return Objects.hash(primaryIsbn10, primaryIsbn13, rank, listName, displayName, publishedDate, bestsellersDate, weeksOnList, asterisk, dagger);
  }


  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append("class BookRanksHistory {\n");
    
    sb.append("    primaryIsbn10: ").append(toIndentedString(primaryIsbn10)).append("\n");
    sb.append("    primaryIsbn13: ").append(toIndentedString(primaryIsbn13)).append("\n");
    sb.append("    rank: ").append(toIndentedString(rank)).append("\n");
    sb.append("    listName: ").append(toIndentedString(listName)).append("\n");
    sb.append("    displayName: ").append(toIndentedString(displayName)).append("\n");
    sb.append("    publishedDate: ").append(toIndentedString(publishedDate)).append("\n");
    sb.append("    bestsellersDate: ").append(toIndentedString(bestsellersDate)).append("\n");
    sb.append("    weeksOnList: ").append(toIndentedString(weeksOnList)).append("\n");
    sb.append("    asterisk: ").append(toIndentedString(asterisk)).append("\n");
    sb.append("    dagger: ").append(toIndentedString(dagger)).append("\n");
    sb.append("}");
    return sb.toString();
  }

  /**
   * Convert the given object to string with each line indented by 4 spaces
   * (except the first line).
   */
  private String toIndentedString(java.lang.Object o) {
    if (o == null) {
      return "null";
    }
    return o.toString().replace("\n", "\n    ");
  }

}

